package com.truckla.cars.security;

import org.springframework.boot.actuate.autoconfigure.web.server.ManagementServerProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings("PMD")
public class InsecureHealthController {
    private final ManagementServerProperties management;

    public InsecureHealthController(ManagementServerProperties management) {
        this.management = management;
    }

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public void health(final HttpServletRequest request,
            final HttpServletResponse response) throws ServletException, IOException {
        final String healthUrl = UriComponentsBuilder.fromPath(management.getServlet().getContextPath())
                                                     .path("/aws-health").toUriString();
        final HttpServletRequestWrapper requestWrapper = new HttpServletRequestWrapper(request);
        request.getRequestDispatcher(healthUrl).forward(requestWrapper, response);
    }
}
